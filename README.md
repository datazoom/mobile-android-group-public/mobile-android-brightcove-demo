# mobile-android-brightcove
This Android project is a sample application to demonstrate the usage of [Datazoom](https://www.datazoom.io/ "Title")'s Android Brightcove Player collector.
Datazoom’s Brightcove Collector facilitates Brightcove android applications to send video playback events based on the configuration created in data-pipes.

### Running demo application in emulator/mobile device
Follow the steps to run the application in emulator/mobile device

1. Clone the repository

```
git clone https://gitlab.com/datazoom/mobile-android-group/mobile-android-brightcove-demo.git
```
2. Open mobile-android-brightcove-demo in Android Studio
3. Build and Run the application


### Demo application
A demo application can be found [here](https://gitlab.com/datazoom/mobile-android-group/mobile-android-demo-apk/tree/master/brightcove-demo).

You can find the latest version by looking into the version code.

Download and install the application


#### Start using the application

Open the application

The application shall show two input text boxes

1. The first one is to input the url from where the configuration has to be fetched. 
2. The second one is the configuration id which has to be used

Enter the required configuration id and url and click the submit button to use the application for capturing events. (It may take a few minutes initially for the video to load from the internet. Please wait during this time)

## Usage of collector library

- You can find the usage instructions from the below url
- https://datazoom.atlassian.net/wiki/spaces/EN/pages/322602338/Brightcove+Android

### References
https://support.brightcove.com/overview-brightcove-player-sdk-android

## Credits

 - Shyam - Developed the base framework that can be used for all the players.
 - Roke Borgio - Implemented the brightcove collector.
 - Sreekutty - Implemented Brightcove Sample app.

## Link to License/Confidentiality Agreement
 Datazoom, Inc ("COMPANY") CONFIDENTIAL
 Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
 herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

 The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
 information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
 OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
 LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.